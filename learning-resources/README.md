# TangibleAI Resource Repository

## Table of Contents
1. [Command Line](#command-line)
2. [Conda](#conda)
3. [Dyshel](#dyshel)
4. [Git](#git)
5. [GitLab](#gitlab)
6. [Hobson](#hobson)
7. [Markdown](#markdown)
8. [TangibleAI](#tangibleAI)
9. [Qary](#qary)


### [Command Line](./commandline/readme.md)
[Table of Contents](./commandline/readme.md#table-of-contents)
### [Conda](./conda/readme.md)
[Table of Contents](./conda/readme.md#table-of-contents)
### [Dyshel](/dyshel/readme.md)
[Table of Contents](./dyshel/readme.md#table-of-contents)
### [Git](./git/readme.md)
[Table of Contents](./git/readme.md#table-of-contents)
### [GitLab](./gitlab/readme.md)
[Table of Contents](./gitlab/readme.md#table-of-contents)
### [Hobson](./hobson/readme.md)
[Table of Contents](./hobson/readme.md#table-of-contents)
### [Markdown](./markdown/readme.md)
[Table of Contents](./markdown/readme.md#table-of-contents)
### [TangibleAI](./tangibleai/readme.md)
[Table of Contents](./tangibleai/readme.md#table-of-contents)
### [Qary](./qary/readme.md)
[Table of Contents](./qary/readme.md#table-of-contents)

### [resources.yml](./resources.yml)

Example yml video schema:

```
-
    title: name of the Video or Document
    url: associated digitalocean url
    duration: approximate duration of video
    authors: contributing authors of the work
    tags:
        - first tag denotes the resource folder (or primary topic) of the video
        - second tag denotes the digital ocean source folder
        - additional tags to denote keywords.
-
```


