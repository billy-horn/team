# Git Resources & Content

## Table of Contents
1. [What is Git?](#what-is-git?)
    a. [TangibleAI.git](#tangibleai.git)
    b. [GitLab](#gitlab)
2. [Install](#how-to-install)


###  What is Git? 

Git is the most popular version control system of the world. Please check out this video to learn more! 

[@imageg01](https://www.youtube.com/watch?v=2ReR1YJrNOM)

#### TangibleAI.git

Our Git repository is hosted within [GitLab](@gitlab) and can be found here. 
[TangibleAI git repository](https://gitlab.com/tangibleai)

#### GitLab 

[GitLab](@gitlab) is an opensource framework for Git that we use exclusively. It is free and many of our resources for it can be found [here](@gitlab).

### How to Install

#### Windows/Linux/Mac
Installation of Git is relatively painless and system agnostic. Clear instructions on how to install can be found [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)