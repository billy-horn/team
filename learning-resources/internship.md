# Internship

What are the practical skills that interns would like to take away from an unpaid educational internship?

* Programming basics:
    * Importing modules
    * conditions
    * loops
* Basic ML understanding (also AI, from big picture perspective)
* Basic web development (HTTP requests, REST APIs, JSON)
* Coding best practices, patterns, anti-patterns, style, linters, and auto-formatters
* Basic of chatbot building: intents, entities, utterances

* Company and vision:
    * Introduction to Tangible AI
    * "How nonprofits use AI" webinar
    * Open source principles
    * Open source licenses and how to choose the right one

## Checklist

This checklist is interactive within gitlab or github, I think.
So students may be able to keep track of their progress within a fork of this repository.
And a bot may be able to follow their progress by tracking their

Here's a draft schedule of what you can expect for the next 10 weeks: [internship-syllabus.md]

## Project Ideas (10 weeks)

### Basic

- Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles)
    - [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)
- elasticcloud as repository for scraped tweets
- Tweet scraper targeting hard-coded list of nonprofit relevant hashtags
- named entity extraction and identification from personal tweets (from list of small nonprofits)
- bot and ad filtering/identificaiton
- sentiment analysis
- summarization of sentiment for each nonprofit brand
- topic analysis for each nonprofit brand
- twitter search from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"

### Computer Science

- spelling corrector
- GED, GEC, GEH
- dialog planner to coach ESL learners in composing correct sentences (Mohammed's webapp)
- Drawing/listening ESL games that could be autmated with a chatbot:
    - [ESL Listening Games](2020-09-esl-writing-skill-coach-high-schoolers.md)
- Grammar correctors/suggester like Grammerly, but we'd like to be more conversational, like a parent or teacher
    - [Free Writing Assistant for Google Docs by ETS (SAT Test prep)](https://mentormywriting.org/)

### ML/NLP

- NLP within qary?
- context planner?
- actions/commands/reminders in qary?
- LSTM/GRU/Transformer to translate English->French/Spanish or rare language like Amharic

### Chatbot Converter (Urgent Need)

#### translate `rapidpro.json` into  `botpress.json`

- RapidPro is designed for SMS so doesn't have real buttons, like botpress.
- Only Rapidpro `message node`s and `router node`s need to be translated
    - with output destination (platform, user) pair of IDs
    - with buttons (called quick-replies in rapidpro)
- Router
    - `has words` intent classifier
    - stretch: `regex` intent classifier
- stretch: intent recognition

#### Tasks:

- [ ] build same simple 2-node (mulitple-choice node in rapidpro) bot with button in each
  - [ ] botpress (`choice`) - 2 pt
  - [ ] rapidpro (`ready-reply`) - 2 pt
- [ ] export exmple of each for choice/reply feature - 2 pt
- [ ] understand schema of each for choice/reply feature, [here]()'s a video walkthrough of rapidpro's schema - 4 pt
  - [ ] change something about each bot and re-export (active learning) - 1 pt
- [ ] json loader for both - .5 pt
- [ ] python func updates empty `botpress.json` schema with list of dicts from rapidpro.json (1 multichoice node)
- [ ] python func updates list of single multichoice node to connect a node to one of the branches in the root multichoice node

## Winter 2021 Projects

### Billy

- [x] Datacamp capstone: Image Classifier Django app for recyclables
- [ ] Deploy and productize the Django App
- [ ] Object detector

### Winston

- [ ] Machine Learning to predict lottery wins for passes to backpack in Enchantments National Forest
- Website to allow pass applicants to select the best parameters for their application
- [ ] Add weather/climage features (ave temp, precip, typical low/high/median temp)
- [ ] Add datetime features (month, week, holiday, weekend, DOW, week of holiday, weekend of holiday)
- [ ] Simulation of competitive dynamics (economics)
- [ ] Solve for equilibrium (perhaps dynamic equilibrium rather than static)

### Hanna

- Machine Translation English<->French
- Machine Translation English<->Amharic

### Jose

- Quiz bot for interns

### John

- Realtime chatbot with Vue.js or Svelte.js and RabbitMQ + Celery?

### Uzi

### Polina

- Wireframes for teacher dashboard

### Winnie

- NMT package
- NLPIA content ch 10
- blog post on Sublime etc


