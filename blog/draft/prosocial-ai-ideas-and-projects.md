# Prosocial Machine Learning

## projects

Several projects can incoporate the prosoial AI material

- [Prosocial Machine Learning (ProML)](https://gitlab.com/hobs/proml) for Packt Publishing
    - scheduled to start in March
    - Maria to lead
- [Data Science for Digital Health]() for UCSD
- [Data Science for Good](https://sandiegotechhub.com/data-science/) at San Diego Tech Hub
    - Hobson to copy material for UCSD DSDH course
- [NLPIA 2nd Ed](https://gitlab.com/hobs/nlpia-manuscript) for Manning.com
    - Maria - lead editor
    - Geoff - transformers and attention
    - Winnie (Ch 10-12) and anything RNN or translation
    - Sylvia (Ch 1-4) and anything Chinese
    - Hanna? Amharic? rare language translation
    - Gary - summarization

## GPT-3

# 2021-01-14 Blog post GPT-3 and the AI Technological and economic singularity

Background: GPT-3 is 10 percent better at the "Zero-Shot" Penn Treebank word-level language model problem than BERT: https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word

- GPT-3 stats
    - 48 transfomer layers
    - 175B learnable parameters (GPT-2 1.5B, BERT 0.4B)
    - pretrained on data unavailable to anyone but big tech
    - requires massive compute resources just to do inference (to **use** the model)
    - only the architecture is open source and it's not innovative (brute force, add layers)
- GPT-3 performance on benchmark tasks
    - GPT-3 wins on 3 out of 11 general language modeling tasks
    - GPT-2 also wins on 3 out of 11 tasks (and it requires 500x fewer parameters)
    - BERT

[GPT-3 (Generative Pretrained Transformer version 3)](https://en.wikipedia.org/wiki/GPT-3): a recurrent neural network or autoregressive model.

## backlog

[Django](https://www.educative.io/) at educative.io (by Mohammed)

## Ethics

- Hedonism
- Deontology
- Consequentialism
    - Utilitarianism
    - Pragmatics
    - Deweyanism

## Machine Learning

- manually create a classifier of dog breeds using heigh weight length and other measurements
    - one table (csv and excel tab) for each breed
    - practice statistics (mean, std, histogram) on folds of the data for 3 widely separated breeds
    - single variate classifier for the 4 breeds
    - manual thresholds based on midpoint between means
    - add "other" class and show how this can have implications at puppy kennels, dog breeds, etc
    - add "mix" class and show how this can have implications on dogs (euthenizing impure breeds, pure breds relegated to breeding kennels, inbreeding and genetic abnormalities, human society affected by the kinds of dogs they have as companions and their attitude towards other humans of mixed ancestry)
    - making a mistake about a rotweiler could be fatal to a child buying that breed accidentally (just need to improve accuracy? or reduce false negatives on rotweillerness)

##
